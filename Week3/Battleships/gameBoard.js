const ships = require('./ships');

const gameBoard = () => {

    const board = {};

    for (i = 0; i < 10; i++) {
        for (j = 0; j < 10; j++) {
            board[i+ ", " +j] = '-';
            }  
        }


    const shipArray = [];
    let shipsObject = {};

    for (i = 0; i < 4; i++) {
        shipArray.push(ships(1));
    }
    for (i = 0; i < 3; i++) {
        shipArray.push(ships(2))
    }
    for (i = 0; i < 2; i++) {
        shipArray.push(ships(3))
    }

    shipArray.push(ships(4))
    
    for(let i=0; i<shipArray.length; i++) {
        shipsObject['Ship'+i] = shipArray[i];
    };
    


    //placeShips 
    // might need some logic later on to check the co-ordinates and ship length match up  
    const placeShips = (ship, ...coordinates) => {
        for (let key in board) {
            for (i = 0; i < coordinates.length; i++) {
                if (key === coordinates[i]) {
                    for(let keys in shipsObject) {
                        if (keys === ship) {
                            board[key] = ship;
                        }
                    }   
                }   
            }
        };
     return board;

    };
  
    //recieveAttack
    const receiveAttack = (coordinates) => {

        for (let key in board) {
            if(key === coordinates) {
                if(board[key] === '-') {
                    board[key] = 'M';
                    return false;
                } else {
                    for(let keys in shipsObject) {
                        if(board[key] === keys) {
                            //shipsObject[keys].hit.hit();
                            //shipsObject.keys.hit.Function;
                            shipsObject[keys].hit;
                        }
                    }

                    return true;
                } 
            }
        }
    };

    //keep track of hits


    //all have been hit 

    return {board, placeShips, receiveAttack, shipsObject}

};

module.exports = gameBoard;